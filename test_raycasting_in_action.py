"""Test module for raycasting."""

import raycaster
import pygame
import math
import time

W, H = 300, 300
LINES = 255, 255, 255
RAYS = 0, 255, 255
POINTS = 255, 0, 0

pygame.display.init()
display = pygame.display.set_mode((W, H))
pygame.display.set_caption("Raycasting in action")

#                        pos     direction     length
line1 = raycaster.Line((0, 0), math.radians(0), 300)
line2 = raycaster.Line((0, 0), math.radians(90), 300)
line3 = raycaster.Line((300, 300), math.radians(-90), 300)
line4 = raycaster.Line((300, 300), math.radians(180), 300)
line5 = raycaster.Line((100, 100), math.radians(20), 100)
lines = [line1, line2, line3, line4, line5]

#                            pos       direction      range       fov          rays
scan = raycaster.Scanner((100, 70), math.radians(60), 1000, math.radians(90), 50)
intersections = scan.scan(lines)

# rendering
pygame.draw.line(display, LINES, line1.o, line1.end, 3)
pygame.draw.line(display, LINES, line2.o, line2.end, 3)
pygame.draw.line(display, LINES, line3.o, line3.end, 3)
pygame.draw.line(display, LINES, line4.o, line4.end, 3)
pygame.draw.line(display, LINES, line5.o, line5.end, 3)
for p in intersections:
    time.sleep(.01)
    pygame.draw.aaline(display, RAYS, scan.o, (int(p[0]), int(p[1])))
    pygame.draw.circle(display, POINTS, (int(p[0]), int(p[1])), 3)
    pygame.display.update()

time.sleep(5)
