"""Test module of a controlled scanner."""

import raycaster
import math
import pygame
import time

pygame.display.init()
W, H = 600, 300
display = pygame.display.set_mode((W, H))
pygame.display.set_caption("Raycasting to 3D controlled view")

#                        pos     direction     length
line1 = raycaster.Line((0, 0), math.radians(0), W//2)
line2 = raycaster.Line((0, 0), math.radians(90), H)
line3 = raycaster.Line((300, 300), math.radians(-90), H)
line4 = raycaster.Line((300, 300), math.radians(180), W//2)
line5 = raycaster.Line((W//6, H//3), math.radians(20), 100)
lines = [line1, line2, line3, line4, line5]

#                            pos       direction      range       fov          rays
scan = raycaster.Scanner((100, 70), math.radians(60), 1000, math.radians(80), 100)


def height(d):
    return 1 / d if d > 1 else 1


def render_scene(scanner, world, background, display_size, wall_h, fish_eye):
    intersects = scanner.scan(world)
    heights = []
    theta = -scanner.fov / 2
    dtheta = scanner.fov / scanner.n
    for p in intersects:
        d = raycaster.euclidian_distance(p, scanner.o) / 30
        if not fish_eye:  # corrects the fish eye effect
            d = d * abs(math.cos(theta))
            theta += dtheta
        heights.append(height(d))

    display = pygame.Surface(display_size)
    display.blit(background, (0, 0))
    d_w, d_h = display_size
    x = 0
    dx = d_w // scanner.n
    print(dx, len(heights), end="\r")

    for h in heights:
        rect = pygame.Rect(x, 0, dx, wall_h * h)
        rect.centery = d_h // 2
        color = int(255*h**2), int(255*h**2), int(255*h**2)
        pygame.draw.rect(display, color, rect)
        x += dx

    return display

def render_map(scanner, world, display_size):
    intersects = scanner.scan(world)

    display = pygame.Surface(display_size)

    for l in world:
        pygame.draw.line(display, (255,255,255), l.o, l.end, 3)

    for p in intersects:
        pygame.draw.aaline(display, (0,255,255), scan.o, (int(p[0]), int(p[1])))
        pygame.draw.circle(display, (255,0,0), (int(p[0]), int(p[1])), 3)

    pygame.draw.circle(display, (0,0,255), (int(scan.o[0]), int(scan.o[1])), 4)

    return display

# rendering
wall_h = H
B_W = W // 2
background = pygame.Surface((B_W, H))
background.fill((157, 157, 157))
pygame.draw.rect(background, (157, 157, 10), pygame.Rect(0, H//2, B_W, H//2))

quit = False
clock = pygame.time.Clock()
forward, backward, left, right, rot_left, rot_right = False, False, False, False, False, False
fish = False

print("w,a,s,d, ->, <-: move")
print("f: toggle fish-eye")
print("q: quit")
while not quit:  # render loop
    for event in pygame.event.get((pygame.KEYDOWN, pygame.KEYUP, pygame.QUIT), True):  # event loop
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                quit = True
            if event.key == pygame.K_f:
                fish = not fish
                print("fish-eye:", fish)
            if event.key == pygame.K_w:
                forward = True
            elif event.key == pygame.K_s:
                backward = True
            if event.key == pygame.K_a:
                left = True
            elif event.key == pygame.K_d:
                right = True
            if event.key == pygame.K_LEFT:
                rot_left = True
            elif event.key == pygame.K_RIGHT:
                rot_right = True
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                forward = False
            elif event.key == pygame.K_s:
                backward = False
            if event.key == pygame.K_a:
                left = False
            elif event.key == pygame.K_d:
                right = False
            if event.key == pygame.K_LEFT:
                rot_left = False
            if event.key == pygame.K_RIGHT:
                rot_right = False
        if event.type == pygame.QUIT:
            quit = True

    if forward:
        scan.o = raycaster.Line(scan.o, scan.d, 1.5).end
    if backward:
        scan.o = raycaster.Line(scan.o, scan.d-math.pi, 1.5).end
    if left:
        scan.o = raycaster.Line(scan.o, scan.d-math.pi/2, 1.5).end
    if right:
        scan.o = raycaster.Line(scan.o, scan.d+math.pi/2, 1.5).end
    if rot_left:
        scan.d += math.radians(-2.2)
    if rot_right:
        scan.d += math.radians(2.2)

    display.blit(render_scene(scan, lines, background, (B_W, H), wall_h, fish), (0, 0))
    display.blit(render_map(scan, lines, (B_W, H)), (B_W, 0))
    pygame.display.update()
    clock.tick(30)
