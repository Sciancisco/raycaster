"""Test module of raycasting to 3D."""

import raycaster
import math
import pygame
import time

pygame.display.init()
W, H = 300, 300
display = pygame.display.set_mode((W, H))
pygame.display.set_caption("Raycasting to 3D")

#                        pos     direction     length
line1 = raycaster.Line((0, 0), math.radians(0), 300)
line2 = raycaster.Line((0, 0), math.radians(90), 300)
line3 = raycaster.Line((300, 300), math.radians(-90), 300)
line4 = raycaster.Line((300, 300), math.radians(180), 300)
line5 = raycaster.Line((100, 100), math.radians(20), 100)
lines = [line1, line2, line3, line4, line5]

#                            pos       direction      range       fov          rays
scan = raycaster.Scanner((100, 70), math.radians(60), 1000, math.radians(70), 60)
intersections = scan.scan(lines)
heights = []
distances = []

def height(d):
    return 1 / d if d > 1 else 1

for p in intersections:
    d = raycaster.euclidian_distance(p, scan.o) / 20
    distances.append(d)
    heights.append(height(d))

# rendering
wall = pygame.Surface((W, H))
background = pygame.Surface((W, H))
background.fill((157, 157, 157))
pygame.draw.rect(background, (157,157,10), pygame.Rect(0, H//2, W, H//2))

display.blit(background, (0,0))
x = 0
for i in range(len(heights)):
    wall.fill((int(255*heights[i]**2),int(255*heights[i]**2),int(255*heights[i]**2)))
    r = wall.get_rect(x=x, h=H*heights[i], centery=W//2)
    x += W // scan.n
    s = wall.get_rect(h=H*heights[i], w=W//scan.n)
    display.blit(wall.subsurface(s), r)
    pygame.display.update()
    time.sleep(.05)
time.sleep(5)
