"""This module contains the necessary structure to implement raycasting."""

from math import cos, sin, atan, sqrt


def euclidian_distance(p1, p2):
    """Calculate the euclidian distance between two 2D points.

    Parameters:
    ----------
    p1: tuple-like
        coordinates of the first point.
    p1: tuple-like
        coordinates of the second point.

    Returns:
    -------
        the euclidian distance between the points

    """
    x1, y1 = p1
    x2, y2 = p2
    return sqrt((x1-x2)**2 + (y1-y2)**2)


class Line:
    """A simple line.

    Representation:
    --------------
        o-----------> d
        |-----r-----|

    """

    def __init__(self, o, d, r):
        """Create a simple line.

        Parameters:
        ----------
        o: tuple-like
            the origin of the line.
        d: angle in radians
            the direction of the line.
        r: number
            the range of the line.

        """
        self.o = o
        self.d = d
        self.r = r

    @property
    def coords(self):
        """Calculate the coordinates.

        Returns:
        -------
            tuple of coordinates of the line

        """
        x, y = self.o
        return self.o, (self.r * cos(self.d) + x, self.r * sin(self.d) + y)

    @property
    def end(self):
        """Calculate the other end.

        Returns:
        -------
            tuple of coordinates of the other end of the line

        """
        x, y = self.o
        return self.r * cos(self.d) + x, self.r * sin(self.d) + y

    @end.setter
    def end(self, coord):
        """Set the other end of the line.

        Parameters:
        ----------
        coord: tuple-like
            the coordinates of the end of the line.

        """
        x, y = coord
        ox, oy = self.o
        dx, dy = x - ox, y - oy
        self.d = atan(dy/dx)
        self.r = sqrt(dx**2 + dy**2)


    def intersects(self, l):
        """Determine the intersetion of two lines, if it exists.

        Parameters:
        ----------
        l: Line
            the other line.

        Returns:
        -------
            tuple of coordinates or None if no intersections

        """
        if self.d == l.d:
            return None

        # from EgoMoose
        # [https://gist.github.com/EgoMoose/f3d22a503e0598c12d1e3926dc72fb19]
        # [https://www.youtube.com/watch?v=c065KoXooSw] (video)
        ox, oy = self.o
        eox, eoy = self.end
        lx, ly = l.o
        elx, ely = l.end
        rx, ry = eox - ox, eoy - oy
        sx, sy = elx - lx, ely - ly
        d = rx * sy - ry * sx
        try:
            u = ((lx - ox) * ry - (ly - oy) * rx) / d
            t = ((lx - ox) * sy - (ly - oy) * sx) / d
        except (ValueError, ZeroDivisionError):
            return None

        if u >= 0 and u <= 1 and t >= 0 and t <= 1:
            return ox + t * rx, oy + t * ry
        else:
            return None


class Scanner:
    """The scanner for raycasting."""

    def __init__(self, o, d, r, fov, n):
        """Create a scanner.

        Parameters:
        ----------
        o: tuple-like
            the position of the scanner.
        d: angle in radians
            the direction of the scanner.
        r: number
            the range of the scanner.
        fov: angle in radians
            the field of view of the scanner.
        n: int
            the number of rays to scan with.

        """
        self.o = o
        self.d = d
        self.r = r
        self.fov = fov
        self.n = n

    def scan(self, lines):
        """Scan the field of view for intersections.

        Parameters:
        ----------
        lines: sequence of Line
            the lines of the wolrd to scan.

        Returns:
        -------
            tuple of every closest intersection points

        """
        dtheta = self.fov / self.n
        theta = self.d - self.fov / 2
        max_theta = self.d + self.fov / 2
        intersects = []
        ray = Line(self.o, theta, self.r)

        while theta < max_theta:
            closest = None
            min_d = None
            for l in lines:
                p = ray.intersects(l)
                if p is None:
                    continue
                x, y = p
                ox, oy = ray.o
                d = sqrt((x - ox)**2 + (y - oy)**2)
                if min_d is None or d < min_d:
                    min_d = d
                    closest = x, y

            if closest is not None:
                intersects.append(closest)
            theta += dtheta
            ray.d = theta

        return tuple(intersects)
